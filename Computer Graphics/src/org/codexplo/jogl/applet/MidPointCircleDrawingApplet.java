package org.codexplo.jogl.applet;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JApplet;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import org.codexplo.jogl.algorithm.MidPointCircleDrawingAlgorithm;
import org.codexplo.jogl.algorithm.Settings;
import org.codexplo.jogl.algorithm.Util;

import com.jogamp.opengl.util.FPSAnimator;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class MidPointCircleDrawingApplet extends JApplet {
	private MidPointCircleDrawingAlgorithm	algorithm;

	private JTextField						txtCenterx;

	private JTextField						txtCentery;

	private JTextField						txtRadius;

	public MidPointCircleDrawingApplet() {

		JPanel panel = new JPanel();
		panel.setName("panel");
		getContentPane().add(panel, BorderLayout.SOUTH);

		JLabel lblCenterX = new JLabel("Center X:");
		lblCenterX.setName("lblCenterX");
		panel.add(lblCenterX);

		txtCenterx = new JTextField();

		txtCenterx.setName("txtCenterx");
		txtCenterx.setText("15");
		panel.add(txtCenterx);
		txtCenterx.setColumns(5);

		JLabel lblCenterY = new JLabel("Center Y :");
		lblCenterY.setName("lblCenterY");
		panel.add(lblCenterY);

		txtCentery = new JTextField();
		txtCentery.setName("txtCentery");
		txtCentery.setText("15");
		panel.add(txtCentery);
		txtCentery.setColumns(5);

		JLabel lblRadius = new JLabel("Radius :");
		lblRadius.setName("lblRadius");
		panel.add(lblRadius);

		txtRadius = new JTextField();
		txtRadius.setName("txtRadius");
		txtRadius.setText("100");
		panel.add(txtRadius);
		txtRadius.setColumns(5);

		JButton btnPlotCircle = new JButton("Plot Circle");
		btnPlotCircle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				do_btnPlotCircle_actionPerformed(event);
			}
		});
		btnPlotCircle.setName("btnPlotCircle");
		panel.add(btnPlotCircle);

		JPanel panel_1 = new JPanel();
		panel_1.setName("panel_1");
		getContentPane().add(panel_1, BorderLayout.NORTH);

		JLabel lblMidPointCircle = new JLabel(
				"Mid Point Circle Drawing Algorithm");
		lblMidPointCircle.setFont(new Font("Verdana", Font.BOLD, 12));
		lblMidPointCircle.setName("lblMidPointCircle");
		panel_1.add(lblMidPointCircle);

		JPanel graphPanel = new JPanel();
		graphPanel.setName("graphPanel");
		getContentPane().add(graphPanel, BorderLayout.CENTER);

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		GLCanvas glcanvas = new GLCanvas(capabilities);

		glcanvas.addGLEventListener(algorithm = new MidPointCircleDrawingAlgorithm());
		glcanvas.setSize(Settings.WIDTH, Settings.HEIGHT);

		FPSAnimator animator = new FPSAnimator(glcanvas, 60);
		animator.start();
		graphPanel.add(glcanvas);
	}

	protected void do_btnPlotCircle_actionPerformed(ActionEvent event) {
		String x = txtCenterx.getText();
		String y = txtCentery.getText();
		String redius = txtRadius.getText();

		if (!(Util.isNumeric(x) && Util.isNumeric(y) && Util.isNumeric(redius))) {
			JOptionPane.showMessageDialog(this, "Please insert numeric input");
		}
		else {
			algorithm.setX(Integer.parseInt(x));
			algorithm.setY(Integer.parseInt(y));
			algorithm.setRadius(Integer.parseInt(redius));
		}
	}
}
