package org.codexplo.jogl.applet;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JApplet;

import com.jogamp.opengl.util.FPSAnimator;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import org.codexplo.jogl.algorithm.MidPointLineDrawingAlgorithm;
import org.codexplo.jogl.algorithm.Settings;
import org.codexplo.jogl.algorithm.Util;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

@SuppressWarnings("serial")
public class MidPointLineDrawingApplet extends JApplet {
	private JTextField						txtX;

	private JTextField						txtY;

	private MidPointLineDrawingAlgorithm	algorithm;

	private JTextField						txtX_1;

	private JTextField						txtY_1;

	public MidPointLineDrawingApplet() {

		JPanel buttonPanel = new JPanel();
		buttonPanel.setName("buttonPanel");
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		JLabel lblX = new JLabel("x1 :");
		lblX.setName("lblX");
		buttonPanel.add(lblX);

		txtX = new JTextField();
		txtX.setText("-150");
		txtX.setName("txtX");
		buttonPanel.add(txtX);
		txtX.setColumns(5);

		JLabel lblY = new JLabel("y1: ");
		lblY.setName("lblY");
		buttonPanel.add(lblY);

		txtY = new JTextField();
		txtY.setText("-100");
		txtY.setName("txtY");
		buttonPanel.add(txtY);
		txtY.setColumns(5);

		JButton btnPlotLine = new JButton("Plot Line");
		btnPlotLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				do_btnPlotLine_actionPerformed(event);
			}
		});

		JLabel lblX_1 = new JLabel("x2:");
		lblX_1.setName("lblX_1");
		buttonPanel.add(lblX_1);

		txtX_1 = new JTextField();
		txtX_1.setText("150");
		txtX_1.setName("txtX_1");
		buttonPanel.add(txtX_1);
		txtX_1.setColumns(5);

		JLabel lblY_1 = new JLabel("y2 :");
		lblY_1.setName("lblY_1");
		buttonPanel.add(lblY_1);

		txtY_1 = new JTextField();
		txtY_1.setText("150");
		txtY_1.setName("txtY_1");
		buttonPanel.add(txtY_1);
		txtY_1.setColumns(5);
		btnPlotLine.setName("btnPlotLine");
		buttonPanel.add(btnPlotLine);

		JPanel graphPanel = new JPanel();
		graphPanel.setName("graphPanel");
		getContentPane().add(graphPanel, BorderLayout.CENTER);

		JPanel namePanel = new JPanel();
		namePanel.setName("namePanel");
		getContentPane().add(namePanel, BorderLayout.NORTH);

		JLabel lblMidPointAlgorithm = new JLabel(
				"Mid Point Line Drawing Algorithm");
		lblMidPointAlgorithm.setFont(new Font("Verdana", Font.BOLD, 12));
		lblMidPointAlgorithm.setName("lblMidPointAlgorithm");
		namePanel.add(lblMidPointAlgorithm);

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		GLCanvas glcanvas = new GLCanvas(capabilities);

		glcanvas.addGLEventListener(algorithm = new MidPointLineDrawingAlgorithm());
		glcanvas.setSize(Settings.WIDTH, Settings.HEIGHT);

		FPSAnimator animator = new FPSAnimator(glcanvas, 60);
		animator.start();
		graphPanel.add(glcanvas);
	}

	protected void do_btnPlotLine_actionPerformed(ActionEvent event) {
		String x1 = txtX.getText();
		String x2 = txtX_1.getText();
		String y1 = txtY.getText();
		String y2 = txtY_1.getText();

		if (!(Util.isNumeric(x1) && Util.isNumeric(x2) && Util.isNumeric(y1) && Util
				.isNumeric(y2))) {
			JOptionPane.showMessageDialog(this, "Please insert numeric input");
		}
		else {
			algorithm.setX0(Integer.parseInt(x1));
			algorithm.setXn(Integer.parseInt(x2));
			algorithm.setY0(Integer.parseInt(y1));
			algorithm.setYn(Integer.parseInt(y2));
		}
	}
}
