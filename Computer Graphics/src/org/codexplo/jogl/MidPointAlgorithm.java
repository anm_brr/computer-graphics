package org.codexplo.jogl;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

public class MidPointAlgorithm implements GLEventListener {

	private void midPointCirle(int radius, int xC, int yC, GL2 gl) {
		int p;
		int x, y;
		p = 3 - (2 * radius);

		x = 0;

		y = radius;

		drawPoint(x, y, xC, yC, gl);
		while (x <= y) {
			x++;
			if (p < 0) {
				p += 4 * x + 6;
			}
			else {
				p += 4 * (x - y) + 10;
				y--;
			}
			drawPoint(x, y, xC, yC, gl);
		}
	}

	private void drawPoint(int x, int y, int xC, int yC, GL2 gl) {

		putpixel(xC + x, yC + y, 1, gl);

		putpixel(xC + x, yC - y, 1, gl);

		putpixel(xC - x, yC + y, 1, gl);

		putpixel(xC - x, yC - y, 1, gl);

		putpixel(xC + y, yC + x, 1, gl);

		putpixel(xC - y, yC + x, 1, gl);

		putpixel(xC + y, yC - x, 1, gl);

		putpixel(xC - y, yC - x, 1, gl);
	}

	private void putpixel(int i, int j, int k, GL2 gl) {
		gl.glPointSize(3.0f);
		gl.glColor3f(i, j, k);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex2i(i, j);
		gl.glEnd();
	}

	@SuppressWarnings("unused")
	private void midPointLine(int x0, int y0, int x1, int y1, int value, GL2 gl) {

		int x, y, dx, dy, d, j, s1, s2, flag = 0;

		x = x0;
		y = y0;
		// keep track of rise and run - they tell us which way to draw
		dx = Math.abs(x1 - x0);
		dy = Math.abs(y1 - y0);
		// these will track drawing directions and we will not need to x++, y--,
		// etc, in the
		// drawing loop
		s1 = sign(x1 - x0);
		s2 = sign(y1 - y0);

		if (dy > dx) { // swap (dy,dx);
			int temp = dy;
			dy = dx;
			dx = temp;
			flag = 1;
		}

		d = 2 * dy - dx;

		for (j = 1; j <= dx; j++) {
			
			putpixel(x, y, value, gl);
			
			while (d > 0) {
				if (flag == 1) {
					x += s1;
				}
				else {
					y += s2;
				}
				d -= 2 * dx;
			}
			if (flag == 1) {
				y += s2;
			}
			else {
				x += s1;
			}
			d += 2 * dy;
		}
	}

	private int sign(int n) {
		if (n > 0)
			return 1;
		else if (n < 0)
			return (-1);
		return 0;
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		final GL2 gl = drawable.getGL().getGL2();

		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		// midPointLine(10, -120, -100, 120, 1, gl);

		midPointCirle(100, -10, -10, gl);

		gl.glFlush();
	}

	@Override
	public void dispose(GLAutoDrawable arg0) {

	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		GLU glu = new GLU();

		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gl.glLineWidth(3.0f);

		gl.glLoadIdentity();
		glu.gluOrtho2D(-250.0, 250.0, -150.0, 150.0);
	}

	@Override
	public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3,
			int arg4) {

	}

}
