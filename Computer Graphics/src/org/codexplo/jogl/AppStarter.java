package org.codexplo.jogl;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;

import org.codexplo.jogl.algorithm.Settings;

public class AppStarter {

	public static void main(String[] args) {
		try {
			EventQueue.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					new GLWindow(
							new org.codexplo.jogl.algorithm.GraphPaper(),
							"Mid Point Algorithm", Settings.WIDTH,
							Settings.HEIGHT );
				}
			});
		}
		catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
