package org.codexplo.jogl;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

public class LineDrawing implements GLEventListener {

	@Override
	public void display(GLAutoDrawable drawable) {
		final GL2 gl = drawable.getGL().getGL2();

		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		drawLine(10, -120, -100, 120, gl);
		gl.glFlush();
	}

	public void drawLine(int x0, int y0, int x1, int y1, GL2 gl) {
		int dx = Math.abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
		int dy = -Math.abs(y1 - y0), sy = y0 < y1 ? 1 : -1;

		int err = dx + dy, e2;
		for (;;) { /* loop */
			setPixel(x0, y0, gl);
			e2 = 2 * err;
			if (e2 >= dy) { /* e_xy+e_x > 0 */
				if (x0 == x1)
					break;
				err += dy;
				x0 += sx;
			}
			if (e2 <= dx) { /* e_xy+e_y < 0 */
				if (y0 == y1)
					break;
				err += dx;
				y0 += sy;
			}
		}
	}

	private void setPixel(int x0, int y0, GL2 gl) {
		gl.glPointSize(3.0f);
		gl.glColor3f(1, 1, 1);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex2i(x0, y0);
		gl.glEnd();
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {

	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		GLU glu = new GLU();

		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gl.glLineWidth(3.0f);

		gl.glLoadIdentity();
		glu.gluOrtho2D(-250.0, 250.0, -150.0, 150.0);
	}

	@Override
	public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3,
			int arg4) {

	}

}
