package org.codexplo.jogl.algorithm;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

public class GraphPaper extends GLTemplate {

	private void drawGraph(GL2 gl) {
		float red;
		float green;
		float blue;

		gl.glClear(GL.GL_COLOR_BUFFER_BIT);

		// drawing the grid
		red = 0.2f;
		green = 0.2f;
		blue = 0.2f;

		gl.glColor3f(red, green, blue);

		// You may notice I'm using GL_LINES here.
		// Details of glBegin() will be discussed later.
		gl.glBegin(GL.GL_LINES);

		// draw the vertical lines
		for (int x = -Settings.WIDTH / 2; x <= Settings.WIDTH / 2; x += 5) {
			gl.glVertex2d(x, -Settings.WIDTH / 2);
			gl.glVertex2d(x, Settings.WIDTH / 2);
		}

		// draw the horizontal lines
		for (int y = -Settings.HEIGHT / 2; y <= Settings.HEIGHT / 2; y += 5) {
			gl.glVertex2d(-Settings.WIDTH / 2, y);
			gl.glVertex2d(Settings.WIDTH / 2, y);
		}

		gl.glEnd();

		// draw the x-axis and y-axis
		red = 1.0f;
		green = 0.2f;
		blue = 0.4f;

		gl.glColor3f(red, green, blue);

		gl.glBegin(GL.GL_LINES);

		// line for y-axis
		gl.glVertex2d(Settings.WIDTH / 2, Settings.HEIGHT);
		gl.glVertex2d(Settings.WIDTH / 2, -Settings.HEIGHT);

		// line for x-axis
		gl.glVertex2d(Settings.WIDTH, Settings.HEIGHT / 2);
		gl.glVertex2d(-Settings.WIDTH, Settings.HEIGHT / 2);

		gl.glEnd();

		// draw the x-axis and y-axis
		red = 0.0f;
		green = 0.2f;
		blue = 1f;

		gl.glColor3f(red, green, blue);

		gl.glBegin(GL.GL_LINES);

		// line for y-axis
		gl.glVertex2d(0, 250);
		gl.glVertex2d(0, -250);

		// line for x-axis
		gl.glVertex2d(250, 0);
		gl.glVertex2d(-250, 0);

		gl.glEnd();

		// draw arrow heads
		gl.glBegin(GL.GL_TRIANGLES);

		gl.glVertex2d(0, 250);
		gl.glVertex2d(-5, 240);
		gl.glVertex2d(5, 240);

		gl.glVertex2d(0, -250);
		gl.glVertex2d(-5, -240);
		gl.glVertex2d(5, -240);

		gl.glVertex2d(250, 0);
		gl.glVertex2d(240, -5);
		gl.glVertex2d(240, 5);

		gl.glVertex2d(-250, 0);
		gl.glVertex2d(-240, -5);
		gl.glVertex2d(-240, 5);

		gl.glEnd();
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		drawGraph(gl);
	}

	public void putPixel(int i, int j, GL2 gl) {
		gl.glPointSize(1.0f);
		gl.glColor3f(1, 1, 1);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex2i(i, j);
		gl.glEnd();
	}

	public void putCenter(int i, int j, GL2 gl) {
		gl.glPointSize(2.0f);
		gl.glColor3f(1, 0, 0);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex2i(i, j);
		gl.glEnd();
	}
}
