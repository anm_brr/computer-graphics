package org.codexplo.jogl.algorithm;

import javax.media.opengl.GLAutoDrawable;

import com.jogamp.opengl.util.gl2.GLUT;

public class MidPointEllipseDrawingAlgorithm extends GraphPaper {

	private int xc, yc, rx, ry;
	private GLUT glut = new GLUT();

	public int getXc() {
		return xc;
	}

	public void setXc(int xc) {
		this.xc = xc;
	}

	public int getYc() {
		return yc;
	}

	public void setYc(int yc) {
		this.yc = yc;
	}

	public int getRx() {
		return rx;
	}

	public void setRx(int rx) {
		this.rx = rx;
	}

	public int getRy() {
		return ry;
	}

	public void setRy(int ry) {
		this.ry = ry;
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);
		drawEllipse(getXc(), getYc(), getRx(), getRy());

		putCenter(getXc(), getYc(), getGl());
		getGl().glColor3d(0, 1, 0.2);
		getGl().glRasterPos3f(getXc(), getYc(), 0);
		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, "( " + getXc() + ", "
				+ getYc() + " )");

		getGl().glRasterPos3f(-220, -220, 0);
		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, " A :  " + getRx()
				+ "\t B: " + getRy());

	}

	public void drawEllipse(int xc, int yc, int rx, int ry) {
		long rx2, ry2, px, py, tworx2, twory2, p;
		int x, y;
		x = 0;
		y = ry;
		rx2 = rx * rx;
		ry2 = ry * ry;
		tworx2 = 2 * rx2;
		twory2 = 2 * ry2;
		px = 0;
		py = (long) (tworx2 * y);

		drawPoints(xc, yc, x, y);
		p = (int) (ry2 - (rx2 * ry) + (0.25 * rx2));
		while (px < py) {
			x++;
			px += twory2;
			if (p < 0)
				p += px + ry2;
			else {
				y = y - 1;
				py -= tworx2;
				p += ry2 + px - py;
			}
			drawPoints(xc, yc, x, y);
		}
		p = (int) ((ry2 * (x + 0.5) * (x + 0.5) + rx2 * (y - 1) * (y - 1) - rx2
				* ry2));
		while (y > 0) {
			y--;
			py -= tworx2;
			if (p > 0)
				p += rx2 - py;
			else {
				x++;
				px += twory2;
				p += rx2 - py + px;
			}
			drawPoints(xc, yc, x, y);
		}
	}

	public void drawPoints(int xc, int yc, int x, int y) {
		putPixel(x + xc, y + yc, getGl());
		putPixel(x + xc, -y + yc, getGl());
		putPixel(-x + xc, y + yc, getGl());
		putPixel(-x + xc, -y + yc, getGl());
	}
}
