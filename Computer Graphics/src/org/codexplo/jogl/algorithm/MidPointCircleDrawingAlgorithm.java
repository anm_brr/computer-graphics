package org.codexplo.jogl.algorithm;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import com.jogamp.opengl.util.gl2.GLUT;

public class MidPointCircleDrawingAlgorithm extends GraphPaper {
	private int		x		= 15, y = 15, radius = 100;

	private GLUT	glut	= new GLUT();

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRadius() {
		return this.radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);
		GL2 gl = drawable.getGL().getGL2();
		midPointCirle(radius, getX(), getY(), gl);
		putCenter(getX(), getY(), gl);

		gl.glColor3d(0, 1, 0.2);
		gl.glRasterPos3f(getX(), getY(), 0);
		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, "( " + getX() + ", "
				+ getY() + " )");
		gl.glRasterPos3f(-220, -220, 0);
		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, " Radius:  "
				+ getRadius());
	}

	private void midPointCirle(int radius, int xC, int yC, GL2 gl) {
		int p;
		int x, y;
		p = 3 - (2 * radius);
		x = 0;
		y = radius;

		drawPoint(x, y, xC, yC, gl);
		while (x <= y) {
			x++;
			if (p < 0) {
				p += 4 * x + 6;
			}
			else {
				p += 4 * (x - y) + 10;
				y--;
			}
			drawPoint(x, y, xC, yC, gl);
		}
	}

	private void drawPoint(int x, int y, int xC, int yC, GL2 gl) {

		putpixel(xC + x, yC + y, gl);

		putpixel(xC + x, yC - y, gl);

		putpixel(xC - x, yC + y, gl);

		putpixel(xC - x, yC - y, gl);

		putpixel(xC + y, yC + x, gl);

		putpixel(xC - y, yC + x, gl);

		putpixel(xC + y, yC - x, gl);

		putpixel(xC - y, yC - x, gl);
	}

	private void putpixel(int i, int j, GL2 gl) {
		gl.glPointSize(1.0f);
		gl.glColor3f(1, 1, 1);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex2i(i, j);
		gl.glEnd();
	}
}
