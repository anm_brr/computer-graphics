package org.codexplo.jogl.algorithm;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

public abstract class GLTemplate implements GLEventListener {

	private GL2 gl;

	@Override
	public void dispose(GLAutoDrawable arg0) {

	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		setGl(gl);
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		GLU glu = new GLU();

		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		// gl.glLineWidth(2.0f);

		gl.glViewport(-Settings.WIDTH / 2, Settings.WIDTH / 2,
				-Settings.HEIGHT / 2, Settings.HEIGHT / 2);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluOrtho2D(-Settings.WIDTH / 2, Settings.WIDTH / 2,
				-Settings.HEIGHT / 2, Settings.HEIGHT / 2);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		System.out.println("reshape() called: x = " + x + ", y = " + y
				+ ", width = " + width + ", height = " + height);
	}

	public GL2 getGl() {
		return gl;
	}

	private void setGl(GL2 gl) {
		this.gl = gl;
	}
}
