package org.codexplo.jogl.algorithm;

public class Util {
	public static boolean isNumeric(String str) {
		return str.matches("-?\\d+(.\\d+)?");
	}
}
