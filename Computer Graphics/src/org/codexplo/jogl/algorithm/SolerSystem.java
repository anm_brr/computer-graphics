package org.codexplo.jogl.algorithm;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;

import com.jogamp.opengl.util.gl2.GLUT;

public class SolerSystem extends GLTemplate {
	int					MoveX	= 0;

	int					MoveY	= 0;

	public static float	xValue	= 0.0f;

	public static float	yValue	= 0.0f;

	public static float	angle	= 0.0f;

	static float		x1[][]	= new float[360][2];

	static float		x2[][]	= new float[360][2];

	static float		x3[][]	= new float[720][2];

	GLUT				glut	= new GLUT();

	GLU					glu		= new GLU();

	private void generateCircle() {
		int i = 0;

		for (i = 0; i < 360; i++) {
			x1[i][0] = (float) (sin(i * 3.1416 / 180) * 3);
			x1[i][1] = (float) (cos(i * 3.1416 / 180) * 3);
		}

		for (i = 0; i < 360; i++) {
			x2[i][0] = (float) sin(i * 3.1416 / 180) * 1;
			x2[i][1] = (float) cos(i * 3.1416 / 180) * 1;
		}

		for (i = 0; i < 720; i++) {
			x3[i][0] = (float) sin(i * 3.1416 / 180) * 5;
			x3[i][1] = (float) cos(i * 3.1416 / 180) * 5;
		}
	}

	@Override
	public void display(GLAutoDrawable drawable) {

		generateCircle();
		GL2 gl = drawable.getGL().getGL2();

		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		gl.glColor3d(1.0, 1.0, 1.0);

		// sun
		gl.glPushMatrix();
		glu.gluLookAt(0.0, 10.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);

		gl.glTranslatef(xValue, 0.0f, yValue);
		gl.glRotatef(angle, 0.0f, 0.0f, 1.0f);
		glut.glutWireSphere(0.5, 15, 15);
		gl.glPopMatrix();

		gl.glPushMatrix();
		glu.gluLookAt(0.0, 10.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
		if (MoveX == 360)
			MoveX = 0;
		gl.glTranslatef(x1[MoveX][1], x1[MoveX][0], 0.0f);
		gl.glRotatef(angle, 0.0f, 0.0f, 1.0f);
		glut.glutWireSphere(0.4f, 15, 15);
		gl.glTranslatef(x2[MoveX][0], x2[MoveX][1], 0.0f);
		glut.glutWireSphere(0.2f, 15, 15);
		gl.glPopMatrix();

		gl.glPushMatrix();
		glu.gluLookAt(0.0, 10.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
		if (MoveY == 720)
			MoveY = 0;
		gl.glTranslatef(x3[MoveY / 2][1], x3[MoveY / 2][0], 0.0f);
		gl.glRotatef(angle, 0.0f, 0.0f, 1.0f);
		glut.glutWireSphere(0.4, 15, 15);
		int i = 0;
		// glBegin(GL_LINE_STRIP);
		gl.glBegin(GL2.GL_QUAD_STRIP);
		for (i = 0; i <= 360; i++) {
			gl.glVertex3d(sin(i * 3.1416 / 180) * 0.5,
					cos(i * 3.1416 / 180) * 0.5, 0);
			gl.glVertex3d(sin(i * 3.1416 / 180) * 0.7,
					cos(i * 3.1416 / 180) * 0.7, 0);
		}
		gl.glEnd();
		gl.glRotated(angle, 0.0, 0.0, 1.0);
		gl.glPopMatrix();

		gl.glFlush();
		animation();
	}

	void animation() {
		angle += 15.0;
		MoveX += 1;
		MoveY += 1;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		super.reshape(drawable, x, y, width, height);
		GL2 gl = drawable.getGL().getGL2();
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glFrustum(-1.0, 1.0, -1.0, 1.0, 1.5, 20.0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
	}
}
