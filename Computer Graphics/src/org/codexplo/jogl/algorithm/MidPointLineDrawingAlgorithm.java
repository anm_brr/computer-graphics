package org.codexplo.jogl.algorithm;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import com.jogamp.opengl.util.gl2.GLUT;

public class MidPointLineDrawingAlgorithm extends GraphPaper {

	private GL2	gl;

	private int	x0		= -150, y0 = -100, xn = 150, yn = 150;

	GLUT		glut	= new GLUT();

	public int getX0() {
		return this.x0;
	}

	public void setX0(int x0) {
		this.x0 = x0;
	}

	public int getY0() {
		return this.y0;
	}

	public void setY0(int y0) {
		this.y0 = y0;
	}

	public int getXn() {
		return this.xn;
	}

	public void setXn(int xn) {
		this.xn = xn;
	}

	public int getYn() {
		return this.yn;
	}

	public void setYn(int yn) {
		this.yn = yn;
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);
		setGl(drawable.getGL().getGL2());
		drawLine(getX0(), getY0(), getXn(), getYn(), getGl());

		gl.glColor3d(0, 1, 0.2);
		gl.glRasterPos3f(getX0(), getY0(), 0);
		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, "( " + getX0() + ", "
				+ getY0() + " )");

		gl.glRasterPos3f(getXn(), getYn(), 0);
		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, "( " + getXn() + ", "
				+ getYn() + " )");
	}

	public void drawLine(int x0, int y0, int xn, int yn, GL2 gl) {

		int dx = Math.abs(xn - x0), sx = x0 < xn ? 1 : -1;
		int dy = -Math.abs(yn - y0), sy = y0 < yn ? 1 : -1;

		int err = dx + dy, e2;
		for (;;) { /* loop */
			setPixel(x0, y0, gl);
			e2 = 2 * err;
			if (e2 >= dy) { /* e_xy+e_x > 0 */
				if (x0 == xn)
					break;
				err += dy;
				x0 += sx;
			}
			if (e2 <= dx) { /* e_xy+e_y < 0 */
				if (y0 == yn)
					break;
				err += dx;
				y0 += sy;
			}
		}
	}

	private void setPixel(int x0, int y0, GL2 gl) {
		gl.glPointSize(1.0f);
		gl.glColor3f(1, 0, 0);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex2i(x0, y0);
		gl.glEnd();
	}

	public GL2 getGl() {
		return gl;
	}

	private void setGl(GL2 gl) {
		this.gl = gl;
	}
}
