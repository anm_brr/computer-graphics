package org.codexplo.jogl;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;

import com.jogamp.opengl.util.FPSAnimator;

@SuppressWarnings("serial")
public class GLWindow extends JFrame {
	public GLWindow(GLEventListener listener, String title, int width,
			int height) {

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		// The canvas is the widget that's drawn in the JFrame
		GLCanvas glcanvas = new GLCanvas(capabilities);
		glcanvas.addGLEventListener(listener);
		glcanvas.setSize(width, height);

		FPSAnimator animator = new FPSAnimator(glcanvas, 60);
		animator.start();
		getContentPane().add(glcanvas);

		// shutdown the program on windows close event
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle(title);
		setLocationRelativeTo(null);
		setSize(width, height);
		setVisible(true);
	}
}
