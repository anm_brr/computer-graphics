package org.codexplo.jogl.frame;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

public class AlgorithmFactory {
	private static Map<Algorithm, AnimationTrigger> map = new HashMap<>();

	private AnimationTrigger current;

	private static AlgorithmFactory factory = null;

	private AlgorithmFactory() {
		map.put(Algorithm.MID_POINT_CIRCLE_DRAWING, new MidPointCircleDrawing());
		map.put(Algorithm.MID_POINT_LINE_DRAWING, new MidPointLineDrawing());
		map.put(Algorithm.MID_POINT_ELLIPSE_DRAWING,
				new MidPointEllipseDrawing());
		map.put(Algorithm.SOLER_SYSTEM, new SolarPanel());
		map.put(Algorithm.STAR_SIMULATION, new Stars());
	}

	public static AlgorithmFactory getFactory() {
		if (factory != null)
			return factory;
		else
			return new AlgorithmFactory();
	}

	public Map<Algorithm, AnimationTrigger> getMap() {
		return map;
	}

	public AnimationTrigger getCurentAlgorithm() {
		return current;
	}

	public JPanel getAlgorithm(Algorithm algorithm) {
		current = getMap().get(algorithm);
		return (JPanel) current;
	}
}
