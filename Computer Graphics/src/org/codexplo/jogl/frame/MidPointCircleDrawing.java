package org.codexplo.jogl.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.codexplo.jogl.algorithm.MidPointCircleDrawingAlgorithm;
import org.codexplo.jogl.algorithm.Settings;

import com.jogamp.opengl.util.FPSAnimator;
import javax.swing.border.TitledBorder;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class MidPointCircleDrawing extends JPanel implements AnimationTrigger {
	/**
	 * 
	 */
	private static final long				serialVersionUID	= 1L;

	private MidPointCircleDrawingAlgorithm	algorithm			= new MidPointCircleDrawingAlgorithm();

	private FPSAnimator						animator;

	private JSlider							sliderCenterX;

	private JSlider							sliderCenterY;

	private JSlider							sliderRadius;

	public MidPointCircleDrawing() {

		this.setLayout(new BorderLayout());
		setPreferredSize(new Dimension(Settings.WIDTH + 16,
				Settings.HEIGHT + 132));
		JPanel panel = new JPanel();
		panel.setName("panel");
		this.add(panel, BorderLayout.SOUTH);
		panel.setPreferredSize(new Dimension(getWidth(), 60));

		JLabel lblCenterX = new JLabel("Center X:");
		lblCenterX.setName("lblCenterX");
		panel.add(lblCenterX);

		sliderCenterX = new JSlider();
		sliderCenterX.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				do_sliderCenterX_stateChanged(e);
			}
		});

		sliderCenterX.setMinimum(-200);
		sliderCenterX.setMaximum(200);
		sliderCenterX.setValue(0);
		sliderCenterX.setName("sliderCenterX");
		panel.add(sliderCenterX);

		JLabel lblCenterY = new JLabel("Center Y :");
		lblCenterY.setName("lblCenterY");
		panel.add(lblCenterY);

		sliderCenterY = new JSlider();
		sliderCenterY.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				do_sliderCenterY_stateChanged(e);
			}
		});
		sliderCenterY.setMinimum(-200);
		sliderCenterY.setMaximum(200);
		sliderCenterY.setValue(0);
		sliderCenterY.setName("sliderCenterY");
		panel.add(sliderCenterY);

		JLabel lblRadius = new JLabel("Radius :");
		lblRadius.setName("lblRadius");
		panel.add(lblRadius);

		sliderRadius = new JSlider();
		sliderRadius.setMinimum(200);
		sliderRadius.setMinimum(0);
		sliderRadius.setValue(100);
		sliderRadius.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				do_sliderRadius_stateChanged(e);
			}
		});
		sliderRadius.setName("sliderRadius");
		panel.add(sliderRadius);

		JPanel panel_1 = new JPanel();
		panel_1.setName("panel_1");
		this.add(panel_1, BorderLayout.NORTH);

		JLabel lblMidPointCircle = new JLabel(
				"Mid Point Circle Drawing Algorithm");
		lblMidPointCircle.setFont(new Font("Verdana", Font.BOLD, 12));
		lblMidPointCircle.setName("lblMidPointCircle");
		panel_1.add(lblMidPointCircle);

		JPanel graphPanel = new JPanel();
		graphPanel.setBorder(new TitledBorder(null, "Mid Point Circle Drawing",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		graphPanel.setName("graphPanel");
		this.add(graphPanel, BorderLayout.CENTER);

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		GLCanvas glcanvas = new GLCanvas(capabilities);

		glcanvas.addGLEventListener(algorithm);
		glcanvas.setSize(Settings.WIDTH, Settings.HEIGHT);

		animator = new FPSAnimator(glcanvas, 60);

		graphPanel.add(glcanvas);

	}

	protected void do_sliderCenterX_stateChanged(ChangeEvent e) {
		algorithm.setX(sliderCenterX.getValue());
	}

	protected void do_sliderCenterY_stateChanged(ChangeEvent e) {
		algorithm.setY(sliderCenterY.getValue());
	}

	protected void do_sliderRadius_stateChanged(ChangeEvent e) {
		algorithm.setRadius(sliderRadius.getValue());
	}

	@Override
	public void stopAnimation() {
		animator.stop();
	}

	@Override
	public void startAnimation() {
		animator.start();
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}
}
