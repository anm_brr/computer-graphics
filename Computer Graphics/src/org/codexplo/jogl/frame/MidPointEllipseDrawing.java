package org.codexplo.jogl.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.codexplo.jogl.algorithm.MidPointEllipseDrawingAlgorithm;
import org.codexplo.jogl.algorithm.Settings;

import com.jogamp.opengl.util.FPSAnimator;

public class MidPointEllipseDrawing extends JPanel implements AnimationTrigger {

	private static final long serialVersionUID = 411617994569476408L;
	private JSlider sliderXc;
	private JSlider sliderYc;
	private JSlider sliderRx;
	private JSlider sliderRy;
	private FPSAnimator animator;

	private MidPointEllipseDrawingAlgorithm algorithm = new MidPointEllipseDrawingAlgorithm();

	public MidPointEllipseDrawing() {
		this.setLayout(new BorderLayout());
		setPreferredSize(new Dimension(Settings.WIDTH + 16,
				Settings.HEIGHT + 132));
		JPanel panel = new JPanel();
		panel.setName("panel");
		this.add(panel, BorderLayout.SOUTH);
		panel.setPreferredSize(new Dimension(getWidth(), 60));

		sliderXc = new JSlider();
		sliderXc.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				algorithm.setXc(sliderXc.getValue());
			}
		});
		sliderXc.setValue(0);
		sliderXc.setMinimum(-200);
		sliderXc.setMaximum(200);
		panel.add(sliderXc);

		sliderYc = new JSlider();
		sliderYc.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				algorithm.setYc(sliderYc.getValue());
			}
		});
		sliderYc.setValue(0);
		sliderYc.setMinimum(-200);
		sliderYc.setMaximum(200);
		panel.add(sliderYc);

		sliderRx = new JSlider();
		sliderRx.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				algorithm.setRx(sliderRx.getValue());
			}
		});
		sliderRx.setValue(150);
		sliderRx.setMinimum(0);
		sliderRx.setMaximum(200);
		panel.add(sliderRx);

		sliderRy = new JSlider();
		sliderRy.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				algorithm.setRy(sliderRy.getValue());
			}
		});
		sliderRy.setValue(100);
		sliderRy.setMinimum(0);
		sliderRy.setMaximum(200);
		panel.add(sliderRy);

		JPanel panel_1 = new JPanel();
		panel_1.setName("panel_1");
		this.add(panel_1, BorderLayout.NORTH);

		JLabel lblMidPointCircle = new JLabel(
				"Mid Point Ellipse Drawing Algorithm");
		lblMidPointCircle.setFont(new Font("Verdana", Font.BOLD, 12));
		lblMidPointCircle.setName("lblMidPointCircle");
		panel_1.add(lblMidPointCircle);

		JPanel graphPanel = new JPanel();
		graphPanel.setBorder(new TitledBorder(null,
				"Mid Point Ellipse Drawing", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		graphPanel.setName("graphPanel");
		this.add(graphPanel, BorderLayout.CENTER);

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		GLCanvas glcanvas = new GLCanvas(capabilities);

		glcanvas.addGLEventListener(algorithm);
		glcanvas.setSize(Settings.WIDTH, Settings.HEIGHT);

		animator = new FPSAnimator(glcanvas, 60);

		graphPanel.add(glcanvas);
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void stopAnimation() {
		animator.stop();
	}

	@Override
	public void startAnimation() {
		animator.start();
	}
}
