package org.codexplo.jogl.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JPanel;

import org.codexplo.jogl.algorithm.Settings;
import org.codexplo.jogl.algorithm.SolerSystem;

import com.jogamp.opengl.util.FPSAnimator;

@SuppressWarnings("serial")
public class SolarPanel extends JPanel implements AnimationTrigger {
	private SolerSystem	algorithm	= new SolerSystem();

	private FPSAnimator	animator	= null;

	public SolarPanel() {
		setPreferredSize(new Dimension(Settings.WIDTH + 16,
				Settings.HEIGHT + 132));
		JPanel graphPanel = new JPanel();
		graphPanel.setName("graphPanel");
		this.add(graphPanel, BorderLayout.CENTER);

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		GLCanvas glcanvas = new GLCanvas(capabilities);

		glcanvas.addGLEventListener(algorithm);
		glcanvas.setSize(Settings.WIDTH, Settings.HEIGHT);

		animator = new FPSAnimator(glcanvas, 60);
		graphPanel.add(glcanvas);
	}

	@Override
	public void stopAnimation() {
		animator.stop();
	}

	@Override
	public void startAnimation() {
		animator.start();
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
