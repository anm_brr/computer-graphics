
package org.codexplo.jogl.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.codexplo.jogl.algorithm.Settings;

import com.jogamp.opengl.util.FPSAnimator;

public class Stars extends JPanel implements AnimationTrigger {

	private static final long								serialVersionUID	= 1L;

	private org.codexplo.jogl.algorithm.Stars	algorithm			= new org.codexplo.jogl.algorithm.Stars();

	private FPSAnimator										animator;

	public Stars() {

		this.setLayout(new BorderLayout());
		setPreferredSize(new Dimension(Settings.WIDTH + 16,
				Settings.HEIGHT + 132));

		JPanel graphPanel = new JPanel();

		graphPanel.setBorder(new TitledBorder(null, "Star Simulation",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		graphPanel.setName("graphPanel");
		this.add(graphPanel, BorderLayout.CENTER);

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		GLCanvas glcanvas = new GLCanvas(capabilities);

		glcanvas.addGLEventListener(algorithm);
		glcanvas.setSize(Settings.WIDTH, Settings.HEIGHT);
		glcanvas.addKeyListener(this);
		animator = new FPSAnimator(glcanvas, 60);

		graphPanel.add(glcanvas);
	}

	@Override
	public void stopAnimation() {
		animator.stop();
	}

	@Override
	public void startAnimation() {
		animator.start();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		algorithm.keyPressed(e);
		System.out.println(this.getClass().getName() + " " + e.getKeyCode());
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}
}
