package org.codexplo.jogl.frame;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.Dialog.ModalExclusionType;
import java.awt.Window.Type;

import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;

import org.codexplo.jogl.algorithm.Settings;

import java.awt.BorderLayout;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MainApplicationWindow {

	private JFrame frmComputerGraphicsSimulation;

	private JPanel algorithmContainer = null;

	private AlgorithmFactory factory;

	private JProgressBar progressBar;

	private JLabel lblPleaseWait;

	public void factorySetup() {
		factory = AlgorithmFactory.getFactory();
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final MainApplicationWindow window = new MainApplicationWindow();
					window.frmComputerGraphicsSimulation.setVisible(true);

					long executionTime;
					new Thread() {
						@Override
						public void run() {
							super.run();
							for (int i = 0; i < 100; i++) {
								window.progressBar.setValue(i);
								window.progressBar.repaint();
								try {
									Thread.sleep(400);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							window.lblPleaseWait
									.setText("\t\tReady........GoTO  Menubar >> Algorithm");
						}
					}.start();

					executionTime = System.currentTimeMillis();
					new Thread() {
						@Override
						public void run() {
							window.factorySetup();
						}
					}.start();
					executionTime = System.currentTimeMillis() - executionTime;

					System.out.println(executionTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainApplicationWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmComputerGraphicsSimulation = new JFrame();
		frmComputerGraphicsSimulation.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				do_frmComputerGraphicsSimulation_keyPressed(e);
			}
		});
		frmComputerGraphicsSimulation.setForeground(SystemColor.activeCaption);
		frmComputerGraphicsSimulation.setType(Type.POPUP);
		frmComputerGraphicsSimulation.setTitle("Computer Graphics Simulation");
		frmComputerGraphicsSimulation.setName("frmComputerGraphicsSimulation");
		frmComputerGraphicsSimulation
				.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		frmComputerGraphicsSimulation.setSize(Settings.WIDTH + 30,
				Settings.HEIGHT + 200);
		frmComputerGraphicsSimulation.setLocationRelativeTo(null);
		frmComputerGraphicsSimulation
				.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		algorithmContainer = new JPanel();
		algorithmContainer.setName("algorithmContainer");
		frmComputerGraphicsSimulation.getContentPane().add(algorithmContainer,
				BorderLayout.CENTER);
		algorithmContainer.setLayout(new BorderLayout());

		progressBar = new JProgressBar();
		progressBar.setName("progressBar");
		algorithmContainer.add(progressBar, BorderLayout.SOUTH);

		lblPleaseWait = new JLabel(
				"\tPlease Wait....................OpenGL Loading.....");
		lblPleaseWait.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPleaseWait.setName("lblPleaseWait");
		algorithmContainer.add(lblPleaseWait, BorderLayout.CENTER);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setName("menuBar");
		frmComputerGraphicsSimulation.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		mnFile.setName("mnFile");
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				do_mntmExit_actionPerformed(arg0);
			}
		});
		mntmExit.setName("mntmExit");
		mnFile.add(mntmExit);

		JMenu mnView = new JMenu("View");
		mnView.setName("mnView");
		menuBar.add(mnView);

		JMenu mnAlgorithm = new JMenu("Algorithm");
		mnAlgorithm.setName("mnAlgorithm");
		menuBar.add(mnAlgorithm);

		JMenuItem mntmMidPointLine = new JMenuItem("Mid Point Line Drawing");
		mntmMidPointLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mntmMidPointLine_actionPerformed(e);
			}
		});
		mntmMidPointLine.setName("mntmMidPointLine");
		mnAlgorithm.add(mntmMidPointLine);

		JMenuItem mntmMidPointCircle = new JMenuItem("Mid Point Circle Drawing");
		mntmMidPointCircle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mntmMidPointCircle_actionPerformed(e);
			}
		});
		mntmMidPointCircle.setName("mntmMidPointCircle");
		mnAlgorithm.add(mntmMidPointCircle);

		JMenuItem mntmMidPointEllipse = new JMenuItem(
				"Mid Point Ellipse Drawing");
		mntmMidPointEllipse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mntmMidPointEllipse_actionPerformed(e);
			}
		});
		mntmMidPointCircle.setName("mntmMidPointEllipse");
		mnAlgorithm.add(mntmMidPointEllipse);

		JMenuItem mntmSolerSystem = new JMenuItem("Soler System");
		mntmSolerSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mntmSolerSystem_actionPerformed(e);
			}
		});
		
		mntmSolerSystem.setName("mntmSolerSystem");
		mnAlgorithm.add(mntmSolerSystem);

		JMenuItem mntmStarSimulation = new JMenuItem("Star Simulation");
		mntmStarSimulation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				do_mntmStarSimulation_actionPerformed(arg0);
			}
		});
		mntmStarSimulation.setName("mntmStarSimulation");
		mnAlgorithm.add(mntmStarSimulation);

		JMenu mnHelp = new JMenu("Help");
		mnHelp.setName("mnHelp");
		menuBar.add(mnHelp);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mntmAbout_actionPerformed(e);
			}
		});
		mntmAbout.setName("mntmAbout");
		mnHelp.add(mntmAbout);

		JMenuItem mntmHelp = new JMenuItem("Help");
		mntmHelp.setName("mntmHelp");
		mnHelp.add(mntmHelp);
	}

	protected void do_mntmExit_actionPerformed(ActionEvent event) {
		int option = JOptionPane.showConfirmDialog(
				frmComputerGraphicsSimulation, "Are you sure to Exit?",
				"Exit?", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE);
		System.out.println(option);
		if (option == 0) {
			System.exit(0);
		}
	}

	protected void do_mntmMidPointLine_actionPerformed(ActionEvent e) {
		setAlgorithm(Algorithm.MID_POINT_LINE_DRAWING);
	}

	protected void do_mntmMidPointCircle_actionPerformed(ActionEvent e) {
		setAlgorithm(Algorithm.MID_POINT_CIRCLE_DRAWING);
	}

	protected void do_mntmMidPointEllipse_actionPerformed(ActionEvent e) {
		setAlgorithm(Algorithm.MID_POINT_ELLIPSE_DRAWING);
	}

	private void setAlgorithm(Algorithm algorithm) {
		AnimationTrigger trigger = factory.getCurentAlgorithm();
		if (trigger != null) {
			trigger.stopAnimation();
		}
		algorithmContainer.removeAll();
		algorithmContainer.repaint();
		algorithmContainer.add(factory.getAlgorithm(algorithm),
				BorderLayout.CENTER);
		frmComputerGraphicsSimulation.revalidate();
		factory.getCurentAlgorithm().startAnimation();
	}

	protected static void do_mntmAbout_actionPerformed(ActionEvent event) {
		try {
			About dialog = new About();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void do_mntmSolerSystem_actionPerformed(ActionEvent e) {
		setAlgorithm(Algorithm.SOLER_SYSTEM);
	}

	protected void do_mntmStarSimulation_actionPerformed(ActionEvent event) {
		setAlgorithm(Algorithm.STAR_SIMULATION);
	}

	protected void do_frmComputerGraphicsSimulation_keyPressed(KeyEvent e) {
		System.out.println(e.getKeyCode());
		factory.getCurentAlgorithm().keyPressed(e);
	}
}
