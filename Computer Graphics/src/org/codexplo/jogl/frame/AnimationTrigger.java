package org.codexplo.jogl.frame;

import java.awt.event.KeyListener;

public interface AnimationTrigger extends KeyListener{
	public void stopAnimation();

	public void startAnimation();
}
