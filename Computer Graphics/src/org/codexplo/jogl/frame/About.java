package org.codexplo.jogl.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class About extends JDialog {

	private final JPanel	contentPanel	= new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			About dialog = new About();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public About() {
		setTitle("About");
		setBounds(100, 100, 460, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new TitledBorder(null, "About",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		scrollPane.setName("scrollPane");
		contentPanel.add(scrollPane);

		JEditorPane dtrpnHelloWold = new JEditorPane();
		scrollPane.setViewportView(dtrpnHelloWold);
		dtrpnHelloWold.setEditable(false);
		dtrpnHelloWold.setContentType("text/html");
		dtrpnHelloWold.setPreferredSize(new Dimension(400, 170));
		dtrpnHelloWold
				.setText("<html><body><p>Computer graphics are graphics created using computers and, more generally, the representation and manipulation of image data by a computer with help from specialized software and hardware.</p>\r\n\r\n<p>This software is writter for simulation of different compter Graphics Algorithm. </p>\r\n\r\n<p>Written by: <b>Bazlur Rahman</b><br/>\r\nBIT012<br/>\r\nInstitute of Information Techonology, University of Dhaka.<br/>\r\nEmail: <a href=\"mailto: anm_brr@live.com\">anm_brr@live.com</a></p><br/>\r\nWebsite: <a href=\"http://codexplo.wordpress.com\">Code Explosion</a>\r\n</body>\r\n</html>");
		dtrpnHelloWold.setName("dtrpnHelloWold");

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_okButton_actionPerformed(e);
			}
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_cancelButton_actionPerformed(e);
			}
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);

	}

	protected void do_okButton_actionPerformed(ActionEvent e) {
		this.dispose();
	}

	protected void do_cancelButton_actionPerformed(ActionEvent e) {
		this.dispose();
	}
}
