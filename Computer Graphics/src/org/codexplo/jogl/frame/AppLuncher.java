package org.codexplo.jogl.frame;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;


public class AppLuncher {
	public static void main(String[] args) {
		try {
			EventQueue.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					new MidPointCircleDrawing();
				}
			});
		}
		catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
